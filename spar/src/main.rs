// Copyright The Touchstone Tools Authors

use std::collections::BTreeMap;
use std::fs;
use std::os::unix::fs::MetadataExt;
use std::path::PathBuf;
use std::process::Command;
use std::str;
use std::thread::sleep;
use std::time::{Duration, SystemTime, UNIX_EPOCH};

use chrono::{Local, TimeZone};
use clap::{Parser, ValueEnum};
use csv::Writer;
use futures::future::join_all;

use tokio::io::Result;

/// spar - system process activity reporter
#[derive(Parser)]
#[command(version, about, long_about = None)]
struct Cli {
    /// Report activity for commands and arguments containing this string, use
    /// multiple times for additional commands
    #[arg(long)]
    comm: Vec<String>,

    /// Display seconds since Unix epoch for time
    #[arg(short = 'E', long)]
    epoch: bool,

    /// Display the command line with arguments
    #[arg(short = 'A', long)]
    full_command: bool,

    /// Set the output data in CSV format
    #[arg(short, long, default_value = "text")]
    output: Option<OutputFormat>,

    /// Report activity for this process identification number, use multiple
    /// times for additional processes
    #[arg(short, long)]
    pid: Vec<u64>,

    /// Set the procfs location if not in standard location
    #[arg(long, default_value = "/proc")]
    procfs: PathBuf,

    /// Report activity for this system user identification number, use
    /// multiple times for additional users
    #[arg(short, long)]
    uid: Vec<i64>,

    /// Report activity only these users' names
    #[arg(long)]
    user: Vec<String>,

    /// Display username instead of UID
    #[arg(short = 'U', long)]
    username: bool,

    /// Number of seconds between measurement intervals
    #[arg(default_value_t = 2)]
    interval: u64,

    /// Number of measurements
    #[arg(default_value_t = 0)]
    count: u64,
}

#[derive(Copy, Clone, PartialEq, Eq, PartialOrd, Ord, ValueEnum)]
enum OutputFormat {
    /// CSV
    Csv,
    /// Plain text
    Text,
}

struct Proc {
    uid: i64,
    stat: ProcStat,
    io: ProcIo,
    status: ProcStatus,
    fd_nr: i64,
    cmdline: String,
}

struct ProcIo {
    rchar: i64,
    wchar: i64,
    syscr: i64,
    syscw: i64,
    read_bytes: i64,
    write_bytes: i64,
    cancelled_write_bytes: i64,
}

struct ProcStat {
    blkio_ticks: u64,
    comm: String,
    gtime: u64,
    maj_flt: u64,
    min_flt: u64,
    rss: u64,
    stime: u64,
    threads: u64,
    utime: u64,
    vsize: u64,
}

struct ProcStatus {
    voluntary_ctxt_switches: u64,
    nonvoluntary_ctxt_switches: u64,
}

#[tokio::main]
async fn main() -> Result<()> {
    let cli = Cli::parse();

    let output = Command::new("getconf")
        .arg("CLK_TCK")
        .output()
        .expect("could not get HZ");

    let hz: f64 = str::from_utf8(&output.stdout)
        .expect("could not read getconf output")
        .trim()
        .parse::<f64>()
        .expect("could not convert CLK_TCK to float");

    let output = Command::new("getconf")
        .arg("PAGE_SIZE")
        .output()
        .expect("could not get HZ");

    let pagesize: u64 = str::from_utf8(&output.stdout)
        .expect("could not read getconf output")
        .trim()
        .parse::<u64>()
        .expect("could not convert PAGE_SIZE to float");

    let output = Command::new("getconf")
        .arg("_PHYS_PAGES")
        .output()
        .expect("could not get _PHYS_PAGES");

    let phys_pages: f64 = str::from_utf8(&output.stdout)
        .expect("could not read getconf output")
        .trim()
        .parse::<f64>()
        .expect("could not convert _PHYS_PAGES to float");

    let mut processes: [BTreeMap<u64, Proc>; 2] = [BTreeMap::new(), BTreeMap::new()];
    let mut index = 0;
    let mut wtr = Writer::from_writer(std::io::stdout());

    // Setting iteration to 0 let's it run twice because a diff is needed to be calculated before
    // showing anything.
    let mut iteration: u64 = 0;
    match cli.output {
        Some(OutputFormat::Csv) => {
            let _ = wtr.write_record([
                "TIME",
                "PID",
                if cli.username { "USERNAME" } else { "UID" },
                "%USER",
                "%SYS",
                "%GUEST",
                "%WAIT",
                "%CPU",
                "MINFLT/S",
                "MAJFLT/S",
                "VSZ",
                "RSS",
                "%MEM",
                "RIOPS",
                "WIOPS",
                "IOPS",
                "KiB_RD/S",
                "KiB_WR/S",
                "KiB_CCWR/S",
                "THREADS",
                "VCSWCH/S",
                "NCSWCH/S",
                "FD-NR",
                "COMMAND",
            ]);
        }
        _ => {
            println!(
                "{:>11} {:>6} {:>8} {:>5} {:>5} {:>6} {:>5} {:>5} {:>8} {:>8} {:>10} {:>7} {:>5} {:>5} {:>5} {:>5} {:>8} {:>8} {:>10} {:>7} {:>8} {:>8} {:>6} COMMAND",
                "TIME",
                "PID",
                if cli.username { "USERNAME" } else { "UID" },
                "%USER",
                "%SYS",
                "%GUEST",
                "%WAIT",
                "%CPU",
                "MINFLT/S",
                "MAJFLT/S",
                "VSZ",
                "RSS",
                "%MEM",
                "RIOPS",
                "WIOPS",
                "IOPS",
                "KiB_RD/S",
                "KiB_WR/S",
                "KiB_CCWR/S",
                "THREADS",
                "VCSWCH/S",
                "NCSWCH/S",
                "FD-NR",
            );
        }
    }

    loop {
        processes[index].clear();

        let timestamp0 = SystemTime::now()
            .duration_since(UNIX_EPOCH)
            .unwrap()
            .as_millis();

        // Get only PID directories in procfs and filter out unwanted PIDs at the same time.
        let entries = fs::read_dir(&cli.procfs)?
            .filter_map(Result::ok)
            .filter(|e| e.path().is_dir())
            .filter(|e| {
                e.file_name()
                    .to_str()
                    .map(|s| {
                        let is_a_process = s.chars().all(char::is_numeric);
                        let filtered = cli.pid.is_empty()
                            || s.parse::<u64>().ok().map_or(true, |p| cli.pid.contains(&p));
                        is_a_process && filtered
                    })
                    .unwrap_or(false)
            })
            .map(|e| e.path())
            .collect::<Vec<_>>();

        let futures: Vec<_> = entries
            .into_iter()
            .map(|e| {
                let pid = e
                    .file_name()
                    .and_then(|name| name.to_str())
                    .and_then(|name_str| name_str.parse::<u64>().ok())
                    .unwrap();

                tokio::spawn(async move {
                    let stat_future = tokio::fs::read_to_string(e.join("stat")).await;
                    let io_future = tokio::fs::read_to_string(e.join("io")).await;
                    let status_future = tokio::fs::read_to_string(e.join("status")).await;
                    let cmdline_future = tokio::fs::read_to_string(e.join("cmdline")).await;
                    let fd_future = tokio::fs::read_dir(e.join("fd")).await;
                    let uid_future = tokio::fs::metadata(e).await;

                    let statep = if let Ok(data) = stat_future {
                        data
                    } else {
                        return None;
                    };

                    let uid = if let Ok(data) = uid_future {
                        data.uid() as i64
                    } else {
                        -1
                    };

                    let mut stat = ProcStat {
                        blkio_ticks: 0,
                        comm: String::new(),
                        gtime: 0,
                        maj_flt: 0,
                        min_flt: 0,
                        rss: 0,
                        stime: 0,
                        threads: 0,
                        utime: 0,
                        vsize: 0,
                    };

                    // Linux /proc/stat index for reference (subtract 2 because we split on the right of
                    // tcomm since it may contain spaces and the stat file is space delimited otherwise:
                    // 0 pid
                    // 1 tcomm
                    // 2 state
                    // 3 ppid
                    // 4 pgrp
                    // 5 sid
                    // 6 tty_nr
                    // 7 tty_pgrp
                    // 8 flags
                    // 9 min_flt
                    // 10 cmin_flt
                    // 11 maj_flt
                    // 12 cmaj_flt
                    // 13 utime
                    // 14 stime
                    // 15 cutime
                    // 16 cstime
                    // 17 priority
                    // 18 nice
                    // 19 num_threads
                    // 20 it_real_value
                    // 21 start_time
                    // 22 vsize
                    // 23 rss
                    // 24 rsslim
                    // 25 start_code
                    // 26 end_code
                    // 27 start_stack
                    // 28 esp
                    // 29 eip
                    // 30 pending
                    // 31 blocked
                    // 32 sigign
                    // 33 sigcatch
                    // 34 0
                    // 35 0
                    // 36 0
                    // 37 exit_signal
                    // 38 task_cpu
                    // 39 rt_priority
                    // 40 policy
                    // 41 blkio_ticks
                    // 42 gtime
                    // 43 cgtime
                    // 44 start_data
                    // 45 end_data
                    // 46 start_brk
                    // 47 arg_start
                    // 48 arg_end
                    // 49 env_start
                    // 50 env_end
                    // 51 exit_code

                    let mut split = statep.splitn(2, ')');
                    stat.comm.push_str(
                        split
                            .next()
                            .unwrap_or("")
                            .split_once('(')
                            .map(|x| x.1)
                            .unwrap_or(""),
                    );
                    let stats: Vec<&str> = split.next().unwrap_or("").split_whitespace().collect();

                    stat.min_flt = stats[7].parse::<u64>().unwrap();
                    stat.maj_flt = stats[9].parse::<u64>().unwrap();
                    stat.utime = stats[11].parse::<u64>().unwrap();
                    stat.stime = stats[12].parse::<u64>().unwrap();
                    stat.threads = stats[27].parse::<u64>().unwrap();
                    stat.vsize = stats[20].parse::<u64>().unwrap();
                    stat.rss = stats[21].parse::<u64>().unwrap();
                    stat.blkio_ticks = stats[39].parse::<u64>().unwrap();
                    stat.gtime = stats[40].parse::<u64>().unwrap();

                    let mut io = ProcIo {
                        rchar: -1,
                        wchar: -1,
                        syscr: -1,
                        syscw: -1,
                        read_bytes: -1,
                        write_bytes: -1,
                        cancelled_write_bytes: -1,
                    };

                    for line in io_future.unwrap_or_default().lines() {
                        let (key, value) = line.split_once(": ").unwrap();
                        let value = value.parse::<i64>().unwrap();
                        match key {
                            "rchar" => io.rchar = value,
                            "wchar" => io.wchar = value,
                            "syscr" => io.syscr = value,
                            "syscw" => io.syscw = value,
                            "read_bytes" => io.read_bytes = value,
                            "write_bytes" => io.write_bytes = value,
                            "cancelled_write_bytes" => io.cancelled_write_bytes = value,
                            _ => {}
                        }
                    }

                    let mut status = ProcStatus {
                        voluntary_ctxt_switches: 0,
                        nonvoluntary_ctxt_switches: 0,
                    };

                    for line in status_future.unwrap_or_default().lines() {
                        let (key, value) = line.split_once(':').unwrap();
                        match key {
                            "voluntary_ctxt_switches" => {
                                status.voluntary_ctxt_switches =
                                    value.trim().parse::<u64>().unwrap()
                            }
                            "nonvoluntary_ctxt_switches" => {
                                status.nonvoluntary_ctxt_switches =
                                    value.trim().parse::<u64>().unwrap()
                            }
                            _ => {}
                        }
                    }

                    let mut fd_nr = 0;
                    if fd_future.is_err() {
                        fd_nr = -1
                    } else {
                        let mut e = fd_future.unwrap();
                        while let Ok(Some(_)) = e.next_entry().await {
                            fd_nr += 1;
                        }
                    }

                    let cmdline = cmdline_future.unwrap_or_default();

                    Some((
                        pid,
                        Proc {
                            uid,
                            stat,
                            io,
                            status,
                            fd_nr,
                            cmdline,
                        },
                    ))
                })
            })
            .collect();

        let results = join_all(futures).await;
        for (pid, proc) in results
            .into_iter()
            .filter_map(|result| result.ok())
            .flatten()
        {
            processes[index].insert(pid, proc);
        }

        let pids: Vec<u64> = processes[index].keys().cloned().collect();
        for pid in pids {
            let proc = match processes[index].get(&pid) {
                Some(v) => v,
                None => {
                    // Skip if the pid doesn't seem to exist anymore.
                    continue;
                }
            };
            let proc0 = match processes[index ^ 1].get(&pid) {
                Some(v) => v,
                None => {
                    // Skip if there is not previous data to calculate a rate from.
                    continue;
                }
            };

            // Filter out wanted command lines.
            if !cli.comm.is_empty() {
                let mut found = false;
                for c in &cli.comm {
                    if proc.stat.comm.contains(c) || proc.cmdline.contains(c) {
                        found = true;
                        break;
                    }
                }
                if !found {
                    continue;
                }
            }

            // Filter out unwanted UIDs.
            if proc.uid == -1 || (!cli.uid.is_empty() && !cli.uid.contains(&proc.uid)) {
                continue;
            }

            // Filter out unwanted usernames.
            if !cli.user.is_empty()
                && !cli.user.contains(
                    &users::get_user_by_uid(proc.uid as u32)
                        .unwrap()
                        .name()
                        .to_string_lossy()
                        .into_owned(),
                )
            {
                continue;
            }

            let localtime = if cli.epoch {
                (timestamp0 / 1000).to_string()
            } else {
                Local
                    .timestamp_millis_opt(timestamp0 as i64)
                    .unwrap()
                    .format("%H:%M:%S %p")
                    .to_string()
            };

            let utime = ((proc.stat.utime - proc0.stat.utime) as f64 / hz) * 100.0;
            let stime = ((proc.stat.stime - proc0.stat.stime) as f64 / hz) * 100.0;
            let gtime = ((proc.stat.gtime - proc0.stat.gtime) as f64 / hz) * 100.0;
            let blkio_ticks =
                ((proc.stat.blkio_ticks - proc0.stat.blkio_ticks) as f64 / hz) * 100.0;

            let min_flt = (proc.stat.min_flt - proc0.stat.min_flt) as f64 / cli.interval as f64;
            let maj_flt = (proc.stat.maj_flt - proc0.stat.maj_flt) as f64 / cli.interval as f64;

            let vsize = proc.stat.vsize / 1024;
            let rss = proc.stat.rss * pagesize / 1024;

            let voluntary_ctxt_switches =
                (proc.status.voluntary_ctxt_switches - proc0.status.voluntary_ctxt_switches) as f64
                    / cli.interval as f64;
            let nonvoluntary_ctxt_switches = (proc.status.nonvoluntary_ctxt_switches
                - proc0.status.nonvoluntary_ctxt_switches)
                as f64
                / cli.interval as f64;

            let (syscr, syscw, iops, read_bytes, write_bytes, cancelled_write_bytes) =
                if proc.io.syscr == -1 {
                    (-1_i64, -1_i64, -1_i64, -1.0, -1.0, -1.0)
                } else {
                    (
                        proc.io.syscr - proc0.io.syscr,
                        proc.io.syscw - proc0.io.syscw,
                        proc.io.syscr - proc0.io.syscr + proc.io.syscw - proc0.io.syscw,
                        (proc.io.read_bytes - proc0.io.read_bytes) as f64 / 1024.0,
                        (proc.io.write_bytes - proc0.io.write_bytes) as f64 / 1024.0,
                        (proc.io.cancelled_write_bytes - proc0.io.cancelled_write_bytes) as f64
                            / 1024.0,
                    )
                };

            match cli.output {
                Some(OutputFormat::Csv) => {
                    wtr.write_record(&[
                        localtime,
                        pid.to_string(),
                        if cli.username {
                            users::get_user_by_uid(proc.uid as u32)
                                .unwrap()
                                .name()
                                .to_string_lossy()
                                .into_owned()
                        } else {
                            proc.uid.to_string()
                        },
                        utime.to_string(),
                        stime.to_string(),
                        gtime.to_string(),
                        blkio_ticks.to_string(),
                        (utime + stime + blkio_ticks).to_string(),
                        min_flt.to_string(),
                        maj_flt.to_string(),
                        vsize.to_string(),
                        rss.to_string(),
                        (rss as f64 / phys_pages * 100.0).to_string(),
                        syscr.to_string(),
                        syscw.to_string(),
                        (syscr + syscw).to_string(),
                        read_bytes.to_string(),
                        write_bytes.to_string(),
                        cancelled_write_bytes.to_string(),
                        proc.stat.threads.to_string(),
                        voluntary_ctxt_switches.to_string(),
                        nonvoluntary_ctxt_switches.to_string(),
                        proc.fd_nr.to_string(),
                        if cli.full_command {
                            if proc.cmdline.is_empty() {
                                proc.stat.comm.clone()
                            } else {
                                proc.cmdline.clone()
                            }
                        } else {
                            proc.stat.comm.clone()
                        },
                    ])?;
                }
                _ => {
                    println!(
                        "{:>11} {:6} {:>8} {:5.1} {:5.1} {:6.1} {:5.1} {:5.1} {:8.1} {:8.1} {:10} {:7} {:5.1} {:5} {:5} {:5} {:8.1} {:8.1} {:10.1} {:7} {:8.1} {:8.1} {:6} {}",
                        localtime,
                        pid,
                        if cli.username {
                            users::get_user_by_uid(proc.uid as u32)
                                .unwrap()
                                .name()
                                .to_string_lossy()
                                .into_owned()
                        } else {
                            proc.uid.to_string()
                        },
                        utime,
                        stime,
                        gtime,
                        blkio_ticks,
                        utime + stime + gtime + blkio_ticks,
                        min_flt,
                        maj_flt,
                        vsize,
                        rss,
                        rss as f64 / phys_pages * 100.0,
                        syscr,
                        syscw,
                        iops,
                        read_bytes,
                        write_bytes,
                        cancelled_write_bytes,
                        proc.stat.threads,
                        voluntary_ctxt_switches,
                        nonvoluntary_ctxt_switches,
                        proc.fd_nr,
                        if cli.full_command {
                            if proc.cmdline.is_empty() {
                                proc.stat.comm.as_str()
                            } else {
                                proc.cmdline.as_str()
                            }
                        } else {
                            proc.stat.comm.as_str()
                        },
                    );
                }
            }
            wtr.flush()?;
        }

        if cli.count != 0 && iteration >= cli.count {
            break;
        }

        // Account for time taken to gather data.
        let timestamp1 = SystemTime::now()
            .duration_since(UNIX_EPOCH)
            .unwrap()
            .as_millis();
        sleep(Duration::from_millis(
            (cli.interval * 1000) - (timestamp1 - timestamp0) as u64,
        ));

        iteration += 1;
        index ^= 1;
    }

    Ok(())
}
