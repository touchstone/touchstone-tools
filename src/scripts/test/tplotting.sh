#! /bin/sh
#
# Copyright The Touchstone Tools Authors
#

oneTimeSetUp() {
	TESTDIR=$(dirname "$0")
	BINDIR="${TESTDIR}/../../../builds/debug"

	"${BINDIR}/ts-sysstat" -o "$SHUNIT_TMPDIR" -i 1
	RC=$?
	echo "$SHUNIT_TMPDIR"
	assertEquals "stats collection started" 0 $RC
	if [ $RC -ne 0 ]; then
		return 1
	fi

	sleep 8

	"${BINDIR}/ts-sysstat" -o "$SHUNIT_TMPDIR" -s
	RC=$?
	assertEquals "stats collection stop" 0 $RC
}

testGnuplotExists() {
	which gnuplot > /dev/null 2>&1
	RC=$?
	assertEquals "gnuplot found" 0 $RC
}

testPlottingPidstat() {
	"${BINDIR}/ts-plot-pidstat" -i "$SHUNIT_TMPDIR"
	RC=$?
	assertEquals "pidstat plotting" 0 $RC
}

testPlottingSar() {
	"${BINDIR}/ts-plot-sar" -i "$SHUNIT_TMPDIR/sar"
	RC=$?
	assertEquals "pidstat sar" 0 $RC
}

# shellcheck source=/dev/null
. "$(which shunit2)"
