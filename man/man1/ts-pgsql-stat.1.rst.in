===============
 ts-pgsql-stat
===============

---------------------------------------------
PostgreSQL stats collection management script
---------------------------------------------

:Date: @MANDATE@
:Manual section: 1
:Manual group: Touchstone Tools @PROJECT_VERSION@ Documentation
:Version: Touchstone Tools @PROJECT_VERSION@

SYNOPSIS
========

**ts-pgsql-stat** [option...] [command [args...]]

DESCRIPTION
===========

**ts-pgsql-stat** is a wrapper script that starts and stops collecting data
from a PostgreSQL database using **psql**.  This script uses **psql** to
connect to a PostgreSQL database based on the user's environment.  In other
words, setting *PGDATABASE*, *PGHOST*, *PGPORT*, and *PGUSER* controls which
PostgreSQL database stats are collected from.

There are two use cases:

1. Being capturing PostgreSQL statistics until explicitly stopped.
2. Capture PostgreSQL statistics for the duration of the given command.

A snapshot of data is captured from:

* Output from version() function
* List of table names, including schema name
* List of index names, including schema name
* **pg_settings** table, in CSV format

Data is periodically captured in CSV format from:

* **pg_stat_activity** table
* **pg_locks** table
* **pg_stat_database** table
* **pg_statio_all_tables** and **pg_stat_all_tables** tables
* **pg_stat_all_indexes** and **pg_statio_all_indexes** tables

OPTIONS
=======

-d dbname, --dbname=dbname  database name to connect to, default same as what
                            **psql** will use in user's environment
-h hostname, --host=hostname  database server host or docker directory, default
                              same as what **psql** will use in user's
                              environment
-i num  *num* seconds between samples, default 60
-o directory  Location to save data.
-p, --port=port  Database server *port*.
-s, --stop  Stop data collection processes.
-U, --username=username  Database *username*.
-V, --version  Output version information, then exit.
--help  Show this help, then exit.

EXAMPLES
========

Explicitly starting and stopping and stopping data collection (note when
stopping data collection that the directory where data is being collected must
be specified)::

    ts-pgsql-stat -o /tmp/data
    ts-pgsql-stat -o /tmp/data -s

This is an example of collecting data for the duration of the 120 seconds of
the sleep command::

    ts-pgsql-stat -o /tmp/data sleep 120

SEE ALSO
========

**psql**\ (1), **ts-sysstat**\ (1)
