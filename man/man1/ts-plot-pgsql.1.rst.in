===============
 ts-plot-pgsql
===============

--------------------------------
PostgreSQL stats chart generator
--------------------------------

:Date: @MANDATE@
:Manual section: 1
:Manual group: Touchstone Tools @PROJECT_VERSION@ Documentation
:Version: Touchstone Tools @PROJECT_VERSION@

SYNOPSIS
========

**ts-plot-pgsql** [option...]

DESCRIPTION
===========

**ts-plot-pgsql** is a script that uses **gnuplot** to generate charts from the
database, tables, and index statistics tables collected by **ts-pgsql-stat**.
Plots are generated for every database, table and index captured.  The charts
will get create in the subdirectories *db*, *table* and *index* for database,
table, and index statistics, respectively.

OPTIONS
=======

-d dbname  *dbname* to plot stats from, default all databases
-i directory   The *directory* where ts-pgsql-stat collected data.
-o directory   The *directory* to create charts, default is to use the same
               directory as the data files.
-s dimensions  Size of charts, default '1600,1000'
-V, --version  Output version information, then exit.
--help  Show this help, then exit.

SEE ALSO
========

**gnuplot**\ (1), **ts-pgsql-stat**\ (1)
