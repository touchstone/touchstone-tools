#!/bin/sh
usage() {
	cat << EOF
Touchstone Tools (ts-tools) wrapped up in an AppImage.

Usage:
  $(basename "${ARGV0}") COMMAND [OPTION]...

Options:
  The available OPTIONs are dependant on the COMMAND specified.  The -? flag
  will display the available options per COMMAND.

Help commands:
  man   This COMMAND will display the man page for any of the general commands
        below when given as its OPTION.

General commands:
$(find "${APPDIR}/usr/bin" -maxdepth 1 -type f -name "ts-*" -printf "%f\n" | sed -e "s/ts-/  /")

@HOMEPAGE@
EOF
}

if [ $# -eq 0 ]; then
	usage
	exit 1
fi

# Custom argument handling for hopefully most portability.
while [ "${#}" -gt 0 ] ; do
	case "${1}" in
	(-V | --version)
		echo "ts-tools v@PROJECT_VERSION@"
		exit 0
		;;
	(-\? | --help)
		usage
		exit 0
		;;
	(--* | -*)
		echo "$(basename "${ARGV0}"): invalid option -- '${1}'"
		echo "try \"$(basename "${ARGV0}") --help\" for more information."
		exit 1
		;;
	(man)
		if [ "${MANPATH}" = "" ]; then
			export MANPATH="${APPDIR}/usr/share/man"
		else
			export MANPATH="${APPDIR}/usr/share/man:${MANPATH}"
		fi
		man "ts-${2}"
		exit $?
		;;
	(*)
		break
		;;
	esac
	shift
done

export PATH="${APPDIR}/usr/bin:${PATH}"

if [ "${LD_LIBRARY_PATH}" = "" ]; then
	export LD_LIBRARY_PATH="${APPDIR}/usr/lib"
else
	export LD_LIBRARY_PATH="${APPDIR}/usr/lib:${LD_LIBRARY_PATH}"
fi

export PERL5LIB="${APPDIR}/etc/perl"

COMMAND="${APPDIR}/usr/bin/ts-${1}"

if [ ! -f "${COMMAND}" ]; then
	echo "COMMAND '${1}' not found, run '$(basename "${ARGV0}")' without arguments for usage."
	exit 1
fi

shift

$COMMAND "${@}"
